#!/bin/sh

# Copyright 2008, 2009, 2010, 2011  Patrick Volkerding, Sebeka, MN, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ''AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
NAME="sane"
VERSION=1.0.25
BACKVER=1.0.24
FRONTVER=1.0.14
BUILDNUM=${BUILDNUM:-"1"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
LINK=${LINK:-"https://alioth.debian.org/frs/download.php/file/1140/sane-frontends-$FRONTVER.tar.gz"}
LINK1=${LINK1:-"https://alioth.debian.org/frs/download.php/latestfile/176/sane-backends-$BACKVER.tar.gz"}
MAKEDEPENDS="libieee1284 \
	openjpeg \
	gphoto2 \
	v4l-utils \
	"
# Automatically determine the architecture we're building on:
if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) export ARCH=i586 ;;
    arm*) export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
       *) export ARCH=$( uname -m ) ;;
  esac
fi

NUMJOBS=${NUMJOBS:-" -j6 "}
if [ "$NORUN" != 1 ]; then

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  LIBDIRSUFFIX=""
  CONFIGURE_TRIPLET="i586-vector-linux"
elif [ "$ARCH" = "s390" ]; then
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  LIBDIRSUFFIX="64"
  CONFIGURE_TRIPLET="x86_64-vlocity-linux"
else
  SLKCFLAGS="-O2"
  LIBDIRSUFFIX=""
fi

#get the source..........
for SRC in $(echo $LINK $LINK1);do
if [ ! -f $CWD/$(basename $SRC) ]
then
	wget --no-check-certificate -c $SRC
fi
done

CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp
PKG=$TMP/package-sane

rm -rf $PKG
mkdir -p $TMP $PKG

# First, we'll build the backends
cd $TMP
rm -rf sane-backends-$BACKVER
tar xvf $CWD/sane-backends-$BACKVER.tar.gz || exit 1
cd sane-backends-$BACKVER
chown -R root:root .

# Put the SANE_CAP_ALWAYS_SETTABLE definition back until
# everything else catches up with the API change...
 zcat $CWD/patches/sane-frontends-1.0.14-sane_cap_always_settable.diff.gz | patch -p1 || exit 1

CFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --sysconfdir=/etc \
  --mandir=/usr/man \
  --enable-pthread \
  --with-docdir=/usr/doc/sane-$VERSION \
  --localstatedir=/var/lib \
  --enable-locking \
  --with-lockdir=/var/lock/sane \
  --with-group=scanner \
  --enable-libusb_1_0 \
  --build=$CONFIGURE_TRIPLET || exit 1

make $NUMJOBS || make || exit 1
make install || exit 1
make install DESTDIR=$PKG || exit 1


# Add the default udev rules.  Use group "lp" rather than "scanner" to avoid
# breaking CUPS access for multifunction printer/scanner devices (possibly
# the most common type of scanner these days)
#
# Use ACTION!="add|change" to avoid skipping these rules if coming from an
# initrd where udev was started.
mkdir -p $PKG/lib/udev/rules.d
cat tools/udev/libsane.rules \
  | sed -e "s/GROUP=\"scanner\"/GROUP=\"lp\"/g" \
  | sed -e "s/MODE=\"0664\"/MODE=\"0660\"/g" \
  | sed -e "s/ACTION!=\"add\"/ACTION!=\"add|change\"/g" \
  > $PKG/lib/udev/rules.d/80-libsane.rules


# Install the pkgconfig file:
install -D -m644 tools/sane-backends.pc \
     $PKG/usr/lib$LIBDIRSUFFIX/pkgconfig/sane-backends.pc

# Now let's build the frontends:
cd $TMP 
rm -rf sane-frontends-$FRONTVER
tar xvf $CWD/sane-frontends-$FRONTVER.tar.gz || exit 1
cd sane-frontends-$FRONTVER
chown -R root:root .

CFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --sysconfdir=/etc \
  --mandir=/usr/man \
  --with-docdir=/usr/doc/sane-$VERSION \
  --build=$CONFIGURE_TRIPLET || exit 1

make $NUMJOBS || make || exit 1
make install || exit 1
make install DESTDIR=$PKG || exit 1

# Fix stupid permissions:
chown -R root:root $PKG/var
chmod 755 $PKG/var
chmod 1777 $PKG/var/lock
chown root:scanner $PKG/var/lock/sane
chmod 775 $PKG/var/lock/sane

find $PKG | xargs file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null

# List additional backends in /etc/sane.d/dll.conf.
# I don't think it will hurt anything to do this, even
# if these backends turn out not to be available:
zcat $CWD/patches/dll.conf.additions.gz >> $PKG/etc/sane.d/dll.conf
echo "#hpaio" >> $PKG/etc/sane.d/dll.conf

# Move config files:
( cd $PKG/etc/sane.d
  for file in *.conf ; do
    mv $file ${file}.new
  done
)
rm -f /etc/sane.d/*.conf

# Compress and if needed symlink the man pages:
if [ -d $PKG/usr/man ]; then
  ( cd $PKG/usr/man
    for manpagedir in $(find . -type d -name "man*") ; do
      ( cd $manpagedir
        for eachpage in $( find . -type l -maxdepth 1) ; do
          ln -s $( readlink $eachpage ).gz $eachpage.gz
          rm $eachpage
        done
        gzip -9 *.?
      )
    done
  )
fi

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
zcat $CWD/doinst.sh.gz > $PKG/install/doinst.sh

cd $PKG
requiredbuilder -v -y -s $RELEASEDIR $PKG
makepkg -c n -l y $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.txz

rm -rf $TMP
fi
