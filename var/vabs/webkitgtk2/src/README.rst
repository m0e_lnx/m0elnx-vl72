WEBKITGTK2
==========

*DO NOT UPDATE* this package past version 2.4.9.  This package exists
only for compatibility needs for older applications that depend on this
API version of webkitgtk.  Newer applications should use the ``webkitgtk``
package instead of this.
