Harfbuzz
========

- 2015-10-06 - After updating this package, the ``freetype`` package 
  *must* be re-built against the new version.
